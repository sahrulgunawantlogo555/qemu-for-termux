## Cara Pasang Script Ini

- [ ] Setup Script Jangan Lupa
- [ ] Install Bahan Bahanya Dulu
- [ ] Support 32bit Or 64Bit

## Script Cara Install Silakan Copy
- [ ] Nomor 1
```
apt update -y
```

- [ ] Nomor 2
```
apt upgrade -y
```

- [ ] Nomor 3
```
apt install git -y
```

- [ ] Nomor 4
```
git clone https://gitlab.com/sahrulgunawantlogo555/qemu-for-termux
```

## Setelah Itu Kalian Copy Script Ke Internal Storage Dengan Perintah Ini
```
mv qemu-for-termux
```

## Masuk Directory Script Saya Jalankan Script Nya 
```
sh setup.sh
```

## Setelah Sudah Download Bahan Terserah Dari Mana Penting Format Img / Iso Files Setelah Itu Kalian Pindahkan Ke Dalam Folder . qemu-for-termux

## Kalau Sudah Tinggal Jalankan Script Nya Saya Sudah sediakan 2 Script
- [ ] 1 Script Untuk Iso Cdrom
- [ ] 2 Script Untuk Img Hardisk

## Jalankan Script Nya

- [ ] Untuk CDROM ( ISO FILE )
```
sh cdrom.sh
```

- [ ] Untuk HDD (IMG FILE DISK)
```
sh disk.sh
```

